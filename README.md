## PCM2912A based soundcard with TPA3118D2 amplifier

This project contains the schematic and layout for a USB based soundcard with integrated amplifier for regular 4/8 Ohm speakers. As it is designed as a PC audio system it only has an USB input.

~~~
USB -> PCM2912A -> Combo/Mic Jacks -> TPA3118D2 power amplifier -> speaker
                                                 |
100-277 VAC -> Meanwell IRM-60-24 power supply --|
~~~

![compl](img/0_1_compl.jpg)
![dis](img/0_1_dis.jpg)

The internal switches of the audio jack are used so that one can use it with a combo jack headset or with a designated microphone. (The used connectors, see the schematic bom information in Kicad do not have good contact, but this can be solved by a bit of bending) On the front there is a button to put the power amplifier in suspend mode and a mic mute button.

On the backside is a x3 switch for the mic preamp, the gain of the power amp and the output voltage limit (adjust it for 4 / 8 Ohm speakers).

As the case is insulated from PE and there are 230V close to the case I 3D printed a insulating part. If you rebuild the design you are responsible for save operation on your own.

Use my zzLibrary for the schematic symbols and footprints.

### Building it

To solder the TPA3118D2 power amplifier one needs a hot air soldering iron. Alternatively one could use the TPA3116D2 and add an heatsink.

### test results and problems

I've build v0.1. There was a minor pinout error on the audio jack and I changed some component values. Actually it works well and the heatsink capability was good enough when I tested it with a pair of speakers and turned the volume quite high. Maybe I should try with load resistors some time.

A bit annoying is the pop sound every time the device starts to output a sound and goes into suspend again. This is not only the case when you plug the USB in and out, but with every pause of the audio stream e.g. your computer plays a beep: The soundcard turns on and goes into suspend a few seconds after the beep. This was a problem only with Linux (not Windows) but seems to be fixed now by Linux. It does not power off the output anymore.
